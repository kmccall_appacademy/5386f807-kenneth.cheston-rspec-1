
def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, appear=2)
   print ([word] * appear).join(" ")
end

def start_of_word(word, number)
  word.chars[0...number].join.to_s
end

def first_word(string)
  string.split(" ").first
end

def titleize(string)
  little_words = ["and", "in", "the", "over", "of"]
  string = string.split(" ")
  string.each_with_index do |word, idx|
    next if little_words.include?(word) && idx != 0
    word[0] = word[0].upcase!
    string[idx] = word
  end
string.join(" ")
end


def translate(string)
  vowels = "aeoui"
  string = string.split(" ")
  result_array=[]
  string.each do |word|
    word.chars.each_with_index do |letter, idx|
      if vowels.include?(letter) && idx == 0
        result_array << word + "ay"
        break
      end
      if vowels.include?(letter) && word[idx-1] != "q"
        result_array << word[idx..-1] + word[0...idx] + "ay"
        break
      end
    end
  end
  result_array.join(" ")
end

p translate ("square")

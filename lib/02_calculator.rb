

def add(num_1, num_2)
  num_1 + num_2
end

def subtract(num_1, num_2)
  num_1 - num_2
end

def sum(array)
  array.inject(0, :+)
end

def multiply(array)
  array.reduce(:*)
end

def power(num_1, num_2)
  num_1**num_2
end

def factorial(number)
  answer = (1..number).to_a.reduce(:*)
  if answer  == nil
    return 1
  end
answer
end
